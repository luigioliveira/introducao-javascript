let idade = 18

if (idade < 10){
    console.log("Jovem")
} else if (idade >= 10 && idade <= 20){
    console.log("Jovem-adulto")
} else {
    console.log("Velho")
}

classe = idade >= 18 ? "Maior de idade" : "Menor de idade"

console.log(classe)

let numero = 0

while(numero < 10){
    console.log(numero)
    numero++
}

numero = 11

do{
    console.log(numero)
    numero++
} while(numero <= 10)

for(let i=0; i<=10; i++){
    console.log(`Número é ${i}`);
};